
from .preprocessing_extension import MultiColumnLabelEncoder


__all__ = [
    'MultiColumnLabelEncoder'
]
