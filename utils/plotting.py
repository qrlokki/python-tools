import os
import math

import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import tree
from sklearn.metrics import confusion_matrix


def plot_count_and_rate_plot(df, col, hue=None, figsize=(11, 4), **kwargs):
    fig, ax = plt.subplots(1, 2, figsize=figsize)

    sns.countplot(col, data=df, hue=hue, ax=ax[0], **kwargs)
    if hue is not None:
        val_rate = (df.groupby(hue)[[col]].sum() /
                    df.groupby(hue)[[col]].count() )\
            .reset_index()\
            .sort_values([col], ascending=False)
        sns.barplot(x=col, y=hue,
                    data=val_rate, ax=ax[1], **kwargs)

    else:
        val_rate = (df[[col]].sum() / df[[col]].count())
        sns.barplot(data=val_rate, ax=ax[1], **kwargs)
    # display(val_rate)
    plt.tight_layout()
    plt.show()


def plot_boxplot_columns(df,
                         col_list,
                         hue=None,
                         orient="h",
                         col_num=2,
                         figsize=(13, 20),
                         xticks=None):
    """ plotting boxplot from dataframe """

    fig = plt.figure(figsize=figsize)

    for i, c in enumerate(col_list):
        ax = fig.add_subplot(
            math.ceil(len(col_list) / col_num), col_num, i + 1)

        # plot the continent on these axes
        sns.boxplot(x=c, y=hue, data=df, orient=orient, ax=ax)
        ax.set_title(c)
        if xticks:
            ax.get_xaxis().set_major_formatter(xticks)

    fig.tight_layout()
    plt.show()


def plot_swarmplot_columns(df,
                           col_list,
                           hue=None,
                           orient="h",
                           col_num=2,
                           figsize=(13, 20),
                           xticks=None,
                           **kwargs):
    """ plotting swarmplot from dataframe """

    fig = plt.figure(figsize=figsize)

    for i, c in enumerate(col_list):
        ax = fig.add_subplot(
            math.ceil(len(col_list) / col_num), col_num, i + 1)

        # plot the continent on these axes
        sns.swarmplot(x=c, y=hue, data=df,
                      orient=orient, ax=ax, **kwargs)
        ax.set_title(c)
        if xticks:
            ax.get_xaxis().set_major_formatter(xticks)

    fig.tight_layout()
    plt.show()


def plot_distplot_columns(df,
                          col_list,
                          hue=None,
                          col_num=2,
                          figsize=(13, 20),
                          xticks=None,
                          **kwargs):
    """ plotting distplot from dataframe """

    fig = plt.figure(figsize=figsize)

    classes = df[hue].unique()
    for i, c in enumerate(col_list):

        for cls in classes:
            # plot the continent on these axes
            ax = fig.add_subplot(
                math.ceil(len(col_list) / col_num), col_num, i + 1)
            sns.distplot(
                df.loc[df[hue] == cls, c],
                ax=ax,
                kde_kws={"label": cls},
                **kwargs)
            if xticks:
                ax.get_xaxis().set_major_formatter(xticks)
            ax.legend()
            ax.set_title(c)
    fig.tight_layout()

    plt.show()
    plt.clf()
    plt.close('all')


def plot_kdeplot_columns(df,
                         col_list,
                         hue=None,
                         col_num=2,
                         figsize=(13, 20),
                         xticks=None,
                         ** kwargs):
    """ plotting distplot from dataframe """

    fig = plt.figure(figsize=figsize)

    classes = df[hue].unique()
    for i, c in enumerate(col_list):

        for cls in classes:
            # plot the continent on these axes
            ax = fig.add_subplot(
                math.ceil(len(col_list) / col_num), col_num, i + 1)
            sns.kdeplot(
                df.loc[df[hue] == cls, c],
                ax=ax,
                label=cls,
                **kwargs)
            if xticks:
                ax.get_xaxis().set_major_formatter(xticks)
            ax.set_title(c)
    fig.tight_layout()

    plt.show()
    plt.clf()
    plt.close('all')


def plot_label_non_overlapped(fig, ax, label, x, y):
    """
    add non-overlapped label to scatter plot.

    Attribute:
        fig                         figure                    plotting figure object
        ax                         ax                         plotting ax object
        label                      Series                    label
        x                           Series                    x axis
        y                           Series                    y axis
    Return:

    """
    ann = []
    for i, txt in enumerate(label):
        ann.append(ax.annotate(txt, xy=(x[i], y[i])))
    mask = np.zeros(fig.canvas.get_width_height(), bool)

    fig.canvas.draw()

    for a in ann:
        bbox = a.get_window_extent()
        x0 = int(bbox.x0)
        x1 = int(math.ceil(bbox.x1))
        y0 = int(bbox.y0)
        y1 = int(math.ceil(bbox.y1))

        s = np.s_[x0:x1 + 1, y0:y1 + 1]
        if np.any(mask[s]):
            a.set_visible(False)
        else:
            mask[s] = True


def convert_decision_tree_to_ipython_image(clf, feature_names=None, class_names=None,
                                           image_filename=None, tmp_dir=None):
    """ plotting and saving png file decision tree """
    dot_filename = os.path.basename(image_filename).split(".")[0] + ".dot"

    tree.export_graphviz(clf, out_file=dot_filename,
                         feature_names=feature_names,
                         class_names=class_names,
                         filled=True, rounded=True, node_ids=True,
                         special_characters=False)
    from IPython.display import Image
    image_filename = image_filename or ('%s.png' % dot_filename)

    subprocess.call(('dot -Tpng -o %s %s' %
                     (image_filename, dot_filename)).split(' '))
    image = Image(filename=image_filename)

    return image


def plot_confusion_matrix(train_actual,
                          train_pred,
                          test_actual,
                          test_pred,
                          figsize=(6, 3)):
    """ plotting confusion matrix """

    res_dict = {
        "Train": [train_actual, train_pred],
        "Test": [test_actual, test_pred]
    }

    fig, ax = plt.subplots(1, 2, figsize=figsize)

    for i, res in enumerate(res_dict):
        actual = res_dict[res][0]
        pred = res_dict[res][1]

        labels = sorted(list(set(actual)))
        cmx_data = confusion_matrix(actual, pred, labels=labels)

        df_cmx = pd.DataFrame(cmx_data, index=labels, columns=labels)
        sns.heatmap(
            df_cmx, annot=True, square=True, fmt=',d', cmap="PuBu", ax=ax[i])
        ax[i].set_xlabel("predict")
        ax[i].set_ylabel("actual")
        ax[i].set_title(res)

    plt.tight_layout()
    plt.show()
