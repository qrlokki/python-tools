# -*- coding: utf-8 -*-
"""
Calculate pandas statistics.
"""

import pandas as pd

def calc_summary(df):
    """
    Calculate pandas dataframe's statistics and return result dataframe.

    Params
    -------
    df : pandas DataFrame
        A target DataFrame to calculate summary.

    Returns
    --------
    summary : pandas DataFrame
        Dataframe after calculating summary.
    """
    pd.options.display.float_format = '{:.2f}'.format

    summary = pd.DataFrame(index=df.columns)

    summary["type"] = df.dtypes
    summary["count"] = df.count()
    summary["nunique"] = df.nunique()
    summary["null"] = df.isnull().sum()
    summary["min"] = df.min()
    summary["max"] = df.max()
    summary["25%"] = df.quantile(0.25,numeric_only=True)
    summary["50%"] = df.quantile(0.50,numeric_only=True)
    summary["75%"] = df.quantile(0.75,numeric_only=True)
    summary["mean"] = df.mean(numeric_only=True)
    summary["variance"] = df.var(numeric_only=True)
    summary["stddev"] = df.std(numeric_only=True)

    return summary



