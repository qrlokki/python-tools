import pandas as pd
import psycopg2


class DBAccess():

    def __init__(self, host, port, database, user, password):
        self.connection_config = {
            "host": host,
            "port": port,
            "database": database,
            "user": user,
            "password": pass,
        }

    def get_connection(**self.connection_config):
        """ Get db connection """
        conn = psycopg2.connect(**self.connection_config)
        cur = conn.cursor()
        return conn, cur

    def get_tablenames(schema):
        """ Get table names in specified schema """
        connection, cursor = get_connection(**self.connection_config)

        SQL = "SELECT schemaname, tablename FROM pg_tables WHERE schemaname='" + schema + "'"

        tables = pd.read_sql(sql=SQL, con=connection)

        cursor.close()
        connection.close()

        return tables

    def get_table(table=None, sql=None):
        """ Get table data. Specify table name or SQL. """
        connection, cursor = get_connection(**self.connection_config)

        if sql is None and table is None:
            print("Specify table or sql.")
        elif sql is None:
            sql = "SELECT * FROM " + table ";"

        table = pd.read_sql(sql=sql, con=connection)

        cursor.close()
        connection.close()

        return table
